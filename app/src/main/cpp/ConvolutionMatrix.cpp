#include "ConvolutionMatrix.h"

void ConvolutionMatrix::setAll(double value) {
    for (int i = 0; i < CONVOLUTION_MATRIX_SIZE; ++i) {
        for (int j = 0; j < CONVOLUTION_MATRIX_SIZE; ++j) {
            matrix[i][j] = value;
        }
    }
}

void ConvolutionMatrix::applyConfig(double config[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE]) {
    for (int i = 0; i < CONVOLUTION_MATRIX_SIZE; ++i) {
        for (int j = 0; j < CONVOLUTION_MATRIX_SIZE; ++j) {
            matrix[i][j] = config[i][j];
        }
    }
}

ConvolutionMatrix::pointer_to_matrix ConvolutionMatrix::getMatrix() {
    return matrix;
}

double ConvolutionMatrix::getFactor() const {
    return factor;
}

void ConvolutionMatrix::setFactor(double val) {
    this->factor = val;
}

double ConvolutionMatrix::getOffset() const {
    return offset;
}

void ConvolutionMatrix::setOffset(double val) {
    this->offset = val;
}

