#include <jni.h>
#include <string>
#include <vector>

#include "PixelProcessing.cpp"

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_invertPixels(JNIEnv *env,
                                                                                   jobject thiz,
                                                                                   jintArray jPixels) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.invertPixels(buffer, length);
}

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_greyScalePixels(JNIEnv *env,
                                                                                      jobject thiz,
                                                                                      jintArray jPixels) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.grayScalePixels(buffer, length);
}

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_doGamma(JNIEnv *env,
                                                                              jobject thiz,
                                                                              jintArray jPixels,
                                                                              jdouble red,
                                                                              jdouble green,
                                                                              jdouble blue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.doGamma(buffer, length, red, green, blue);
}

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_doColorFilterPixels(
        JNIEnv *env,
        jobject thiz,
        jintArray jPixels,
        jdouble red,
        jdouble green,
        jdouble blue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.doColorFilter(buffer, length, red, green, blue);
}

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_doSepiaToningPixels(
        JNIEnv *env,
        jobject thiz,
        jintArray jPixels,
        jint depth,
        jdouble red,
        jdouble green,
        jdouble blue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.doSepiaToning(buffer, length, depth, red, green, blue);
}

extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_decreaseColorDepthPixels(
        JNIEnv *env,
        jobject thiz,
        jintArray jPixels,
        jint jBitOffset) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.decreaseColorDepth(buffer, length, jBitOffset);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_createContrastPixels(
        JNIEnv *env,
        jobject thiz,
        jintArray jPixels,
        jdouble jValue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.createContrast(buffer, length, jValue);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_brightnessPixels(JNIEnv *env,
                                                                                       jobject thiz,
                                                                                       jintArray jPixels,
                                                                                       jint jValue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.brightnessPixels(buffer, length, jValue);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_gaussianBlurPixels(
        JNIEnv *env, jobject thiz, jintArray jPixels, jint jWidth, jint jHeight) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.gaussianBlurPixels(buffer, jWidth, jHeight);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_sharpenPixels(JNIEnv *env,
                                                                                    jobject thiz,
                                                                                    jintArray jPixels,
                                                                                    jint jWidth,
                                                                                    jint jHeight,
                                                                                    jdouble jWeight) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.sharpenPixels(buffer, jWidth, jHeight, jWeight);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_meanRemovalPixels(JNIEnv *env,
                                                                                        jobject thiz,
                                                                                        jintArray jPixels,
                                                                                        jint jWidth,
                                                                                        jint jHeight) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.meanRemovalPixels(buffer, jWidth, jHeight);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_smoothPixels(JNIEnv *env,
                                                                                   jobject thiz,
                                                                                   jintArray jPixels,
                                                                                   jint jWidth,
                                                                                   jint jHeight,
                                                                                   jdouble jValue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.smoothPixels(buffer, jWidth, jHeight, jValue);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_embossPixels(JNIEnv *env,
                                                                                   jobject thiz,
                                                                                   jintArray jPixels,
                                                                                   jint jWidth,
                                                                                   jint jHeight) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.embossPixels(buffer, jWidth, jHeight);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_engravePixels(JNIEnv *env,
                                                                                    jobject thiz,
                                                                                    jintArray jPixels,
                                                                                    jint jWidth,
                                                                                    jint jHeight) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    PixelProcessing pixelProcessing;
    pixelProcessing.engravePixels(buffer, jWidth, jHeight);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_boostColorPixels(JNIEnv *env,
                                                                                       jobject thiz,
                                                                                       jintArray jPixels,
                                                                                       jint jType,
                                                                                       jdouble jPercent) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.boostColorPixels(buffer, length, jType, jPercent);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_tintImagePixels(JNIEnv *env,
                                                                                      jobject thiz,
                                                                                      jintArray jPixels,
                                                                                      jint jDegree) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.tintImagePixels(buffer, length, jDegree);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_noisePixels(JNIEnv *env,
                                                                                  jobject thiz,
                                                                                  jintArray jPixels) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.noisePixels(buffer, length);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_blackFilterPixels(JNIEnv *env,
                                                                                        jobject thiz,
                                                                                        jintArray jPixels) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.blackFilterPixels(buffer, length);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_snowFilterPixels(
        JNIEnv *env, jobject thiz, jintArray jPixels) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.snowFilterPixels(buffer, length);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_shadingFilterPixels(JNIEnv *env,
                                                                                          jobject thiz,
                                                                                          jintArray jPixels,
                                                                                          jdouble jRed,
                                                                                          jdouble jGreen,
                                                                                          jdouble jBlue) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.shadingFilterPixels(buffer, length, jRed, jGreen, jBlue);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_applySaturationPixels(
        JNIEnv *env, jobject thiz, jintArray jPixels, jint jLevel) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.applySaturationPixels(buffer, length, jLevel);
}
extern "C"
JNIEXPORT void JNICALL
Java_me_asddsajpgnoob_basicimageprocessing_util_BitmapProcessingUtils_applyHuePixels(JNIEnv *env,
                                                                                     jobject thiz,
                                                                                     jintArray jPixels,
                                                                                     jint jLevel) {
    jint *buffer = env->GetIntArrayElements(jPixels, nullptr);
    env->ReleaseIntArrayElements(jPixels, buffer, JNI_ABORT);

    int length = env->GetArrayLength(jPixels);

    PixelProcessing pixelProcessing;
    pixelProcessing.applyHuePixels(buffer, length, jLevel);
}