#ifndef BASICIMAGEPROCESSING_CONVOLUTIONMATRIX_H
#define BASICIMAGEPROCESSING_CONVOLUTIONMATRIX_H

#define CONVOLUTION_MATRIX_SIZE 3

class ConvolutionMatrix {
public:
    typedef double (*pointer_to_matrix)[CONVOLUTION_MATRIX_SIZE];

    void setAll(double value);

    void applyConfig(double config[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE]);

    pointer_to_matrix getMatrix();

    double getFactor() const;

    void setFactor(double val);

    double getOffset() const;

    void setOffset(double val);

private:
    double matrix[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE];

    double factor = 1;
    double offset = 1;
};

#endif //BASICIMAGEPROCESSING_CONVOLUTIONMATRIX_H
