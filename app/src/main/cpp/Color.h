#ifndef BASICIMAGEPROCESSING_COLOR_H
#define BASICIMAGEPROCESSING_COLOR_H

class Color {
public:
    static unsigned int alpha(unsigned int pixel);

    static int red(int pixel);

    static int green(int pixel);

    static int blue(int pixel);

    static int argb(int alpha, int red, int green, int blue);

    static int rgb(int red, int green, int blue);

    static void HSVToRGB(float HSV[3], int RGB[3]);

    static void RGBToHSV(int RGB[3], float HSV[3]);
};

#endif //BASICIMAGEPROCESSING_COLOR_H