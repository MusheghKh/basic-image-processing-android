#ifndef BASICIMAGEPROCESSING_PIXELPROCESSING_H
#define BASICIMAGEPROCESSING_PIXELPROCESSING_H

#include "ConvolutionMatrix.cpp"

class PixelProcessing {
public:
    static const int COLOR_MIN = 0x00;
    static const int COLOR_MAX = 0xFF;

    void invertPixels(int *jPixelsBuffer, int length) const;

    void grayScalePixels(int *jPixelsBuffer, int length) const;

    void doGamma(jint *jPixelsBuffer, jint length, double red, double green, double blue) const;

    void doColorFilter(jint *jPixelsBuffer, jint length, double red, double green, double blue) const;

    void doSepiaToning(jint *jPixelsBuffer, jint length, int depth, double red, double green, double blue) const;

    void decreaseColorDepth(jint *jPixelsBuffer, jint length, int bitOffset) const;

    void createContrast(jint *jPixelsBuffer, jint length, jdouble value) const;

    void brightnessPixels(jint *jPixelsBuffer, jint length, jint value) const;

    void computeConvolution3x3(jint *jPixelsBuffer, int width, int height, ConvolutionMatrix &convolutionMatrix) const;

    void gaussianBlurPixels(jint *jPixelsBuffer, int width, int height) const;

    void sharpenPixels(jint *jPixelsBuffer, int width, int height, double weight) const;

    void meanRemovalPixels(jint *jPixelsBuffer, int width, int height) const;

    void smoothPixels(jint *jPixelsBuffer, int width, int height, jdouble value) const;

    void embossPixels(jint *jPixelsBuffer, int width, int height) const;

    void engravePixels(jint *jPixelsBuffer, int width, int height) const;

    void boostColorPixels(jint *jPixelsBuffer, int length, int type, double percent) const;

    void tintImagePixels(jint *jPixelsBuffer, int length, int degree) const;

    void noisePixels(jint *jPixelsBuffer, int length) const;

    void blackFilterPixels(jint *jPixelsBuffer, int length) const;

    void snowFilterPixels(jint *jPixelsBuffer, int length) const;

    void shadingFilterPixels(jint *jPixelsBuffer, int length, double red, double green, double blue) const;

    void applySaturationPixels(jint *jPixelsBuffer, int length, int level) const;

    void applyHuePixels(jint *jPixelsBuffer, int length, int level) const;
};

#endif //BASICIMAGEPROCESSING_PIXELPROCESSING_H