#include "Color.h"

unsigned int Color::alpha(unsigned int pixel) {
    return pixel >> 24;
}

int Color::red(int pixel) {
    return (pixel >> 16) & 0xFF;
}

int Color::green(int pixel) {
    return (pixel >> 8) & 0xFF;
}

int Color::blue(int pixel) {
    return pixel & 0xFF;
}

int Color::argb(int alpha, int red, int green, int blue) {
    return (alpha << 24) | (red << 16) | (green << 8) | blue;
}

int Color::rgb(int red, int green, int blue) {
    return 0xff000000 | (red << 16) | (green << 8) | blue;
}

void Color::HSVToRGB(float *HSV, int *RGB) {
    double      hh, p, q, t, ff;
    long        i;
    int R, G, B;

    if(HSV[1] <= 0.0) {       // < is bogus, just shuts up warnings
        RGB[0] = HSV[2];
        RGB[1] = HSV[2];
        RGB[2] = HSV[2];
        return;
    }
    hh = HSV[0];
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = HSV[2] * (1.0 - HSV[1]);
    q = HSV[2] * (1.0 - (HSV[1] * ff));
    t = HSV[2] * (1.0 - (HSV[1] * (1.0 - ff)));

    switch(i) {
        case 0:
            R = HSV[2];
            G = t;
            B = p;
            break;
        case 1:
            R = q;
            G = HSV[2];
            B = p;
            break;
        case 2:
            R = p;
            G = HSV[2];
            B = t;
            break;

        case 3:
            R = p;
            G = q;
            B = HSV[2];
            break;
        case 4:
            R = t;
            G = p;
            B = HSV[2];
            break;
        case 5:
        default:
            R = HSV[2];
            G = p;
            B = q;
            break;
    }
    RGB[0] = R;
    RGB[1] = G;
    RGB[2] = B;
}

void Color::RGBToHSV(int *RGB, float *HSV) {
    double      min, max, delta;

    min = RGB[0] < RGB[1] ? RGB[0] : RGB[1];
    min = min  < RGB[2] ? min  : RGB[2];

    max = RGB[0] > RGB[1] ? RGB[0] : RGB[1];
    max = max  > RGB[2] ? max  : RGB[2];

    HSV[2] = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        HSV[1] = 0;
        HSV[0] = 0; // undefined, maybe nan?
        return;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        HSV[1] = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0
        // s = 0, h is undefined
        HSV[1] = 0.0;
        HSV[0] = NAN;                            // its now undefined
        return;
    }
    if( RGB[0] >= max )                           // > is bogus, just keeps compilor happy
        HSV[0] = ( RGB[1] - RGB[2] ) / delta;        // between yellow & magenta
    else
    if( RGB[1] >= max )
        HSV[0] = 2.0 + ( RGB[2] - RGB[0] ) / delta;  // between cyan & yellow
    else
        HSV[0] = 4.0 + ( RGB[0] - RGB[1] ) / delta;  // between magenta & cyan

    HSV[0] *= 60.0;                              // degrees

    if( HSV[0] < 0.0 )
        HSV[0] += 360.0;
}
