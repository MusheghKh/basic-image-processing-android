#include "PixelProcessing.h"

#include <vector>
#include <cmath>

#include "Color.cpp"

void PixelProcessing::invertPixels(int *jPixelsBuffer, int length) const {
    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];
        A = Color::alpha(p);

        R = 255 - Color::red(p);
        G = 255 - Color::green(p);
        B = 255 - Color::blue(p);
        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::grayScalePixels(int *jPixelsBuffer, int length) const {
    const double GS_RED = 0.299;
    const double GS_GREEN = 0.587;
    const double GS_BLUE = 0.114;

    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];
        A = Color::alpha(p);

        R = Color::red(p);
        G = Color::green(p);
        B = Color::blue(p);

        R = G = B = (int) (GS_RED * R + GS_GREEN * G + GS_BLUE * B);

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::doGamma(int *jPixelsBuffer, int length, double red, double green, double blue) const {
    const int MAX_SIZE = 256;
    const double MAX_VALUE_DBL = 255.0;
    const int MAX_VALUE_INT = 255;
    const double REVERSE = 1.0;

    int gammaR[MAX_SIZE];
    int gammaG[MAX_SIZE];
    int gammaB[MAX_SIZE];

    for (int i = 0; i < MAX_SIZE; ++i) {
        gammaR[i] = (int) std::min(
                MAX_VALUE_INT,
                (int) ((MAX_VALUE_DBL * pow(i / MAX_VALUE_DBL, REVERSE / red)) + 0.5)
        );
        gammaG[i] = (int) std::min(
                MAX_VALUE_INT,
                (int) ((MAX_VALUE_DBL * pow(i / MAX_VALUE_DBL, REVERSE / green)) + 0.5)
        );
        gammaB[i] = (int) std::min(
                MAX_VALUE_INT,
                (int) ((MAX_VALUE_DBL * pow(i / MAX_VALUE_DBL, REVERSE / blue)) + 0.5)
        );
    }

    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];

        A = Color::alpha(p);

        R = gammaR[Color::red(p)];
        G = gammaG[Color::green(p)];
        B = gammaB[Color::blue(p)];

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::doColorFilter(jint *jPixelsBuffer, jint length, double red, double green, double blue) const {
    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];
        A = Color::alpha(p);

        R = (int) (Color::red(p) * red);
        G = (int) (Color::green(p) * green);
        B = (int) (Color::blue(p) * blue);

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::doSepiaToning(jint *jPixelsBuffer, jint length, int depth, double red, double green, double blue) const {
    const double GS_RED = 0.3;
    const double GS_GREEN = 0.59;
    const double GS_BLUE = 0.11;

    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];
        A = Color::alpha(p);

        R = Color::red(p);
        G = Color::green(p);
        B = Color::blue(p);

        B = G = R = (int) (GS_RED * R + GS_GREEN * G + GS_BLUE * B);

        R += (depth * red);
        if (R > 255) {
            R = 255;
        }

        G += (depth * green);
        if (G > 255) {
            G = 255;
        }

        B += (depth * blue);
        if (B > 255) {
            B = 255;
        }

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::decreaseColorDepth(jint *jPixelsBuffer, jint length, int bitOffset) const {
    int A, R, G, B;

    int p;

    for (int i = 0; i < length; ++i) {
        p = jPixelsBuffer[i];
        A = Color::alpha(p);

        R = Color::red(p);
        G = Color::green(p);
        B = Color::blue(p);

        R = ((R + (bitOffset / 2)) - ((R + (bitOffset / 2)) % bitOffset) - 1);
        if (R < 0) {
            R = 0;
        }
        G = ((G + (bitOffset / 2)) - ((G + (bitOffset / 2)) % bitOffset) - 1);
        if (G < 0) {
            G = 0;
        }
        B = ((B + (bitOffset / 2)) - ((B + (bitOffset / 2)) % bitOffset) - 1);
        if (B < 0) {
            B = 0;
        }

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::createContrast(jint *jPixelsBuffer, jint length, jdouble value) const {
    int A, R, G, B;
    int pixel;

    double contrast = pow((100 + value) / 100, 2);

    for (int i = 0; i < length; ++i) {
        pixel = jPixelsBuffer[i];

        A = Color::alpha(pixel);

        R = Color::red(pixel);
        R = (int) (((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if (R < 0) {
            R = 0;
        } else if (R > 255) {
            R = 255;
        }

        G = Color::green(pixel);
        G = (int) (((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if (G < 0) {
            G = 0;
        } else if (G > 255) {
            G = 255;
        }

        B = Color::blue(pixel);
        B = (int) (((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if (B < 0) {
            B = 0;
        } else if (B > 255) {
            B = 255;
        }

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::brightnessPixels(jint *jPixelsBuffer, jint length, jint value) const {
    int A, R, G, B;
    int pixel;

    for (int i = 0; i < length; ++i) {
        pixel = jPixelsBuffer[i];

        A = Color::alpha(pixel);
        R = Color::red(pixel);
        G = Color::green(pixel);
        B = Color::blue(pixel);

        R += value;
        if (R > 255) {
            R = 255;
        } else if (R < 0) {
            R = 0;
        }

        G += value;
        if (G > 255) {
            G = 255;
        } else if (G < 0) {
            G = 0;
        }

        B += value;
        if (B > 255) {
            B = 255;
        } else if (B < 0) {
            B = 0;
        }

        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::computeConvolution3x3(jint *jPixelsBuffer,
                                            int width,
                                            int height,
                                            ConvolutionMatrix &convolutionMatrix) const {
    int A, R, G, B;
    int sumR, sumG, sumB;

    int pixels[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE];

    int xCord;
    int yCord;

    for (int i = 0; i < width - 2; ++i) {

        for (int j = 0; j < height - 2; ++j) {

            for (int x = 0; x < CONVOLUTION_MATRIX_SIZE; ++x) {
                for (int y = 0; y < CONVOLUTION_MATRIX_SIZE; ++y) {
                    xCord = i + x;
                    yCord = j + y;
                    pixels[x][y] = jPixelsBuffer[xCord + yCord  * width];
                }
            }

            A = Color::alpha(pixels[1][1]);

            sumR = sumG = sumB = 0;

            ConvolutionMatrix::pointer_to_matrix matrix = convolutionMatrix.getMatrix();
            for (int x = 0; x < CONVOLUTION_MATRIX_SIZE; ++x) {
                for (int y = 0; y < CONVOLUTION_MATRIX_SIZE; ++y) {
                    sumR += Color::red(pixels[x][y]) * matrix[x][y];
                    sumG += Color::green(pixels[x][y]) * matrix[x][y];
                    sumB += Color::blue(pixels[x][y]) * matrix[x][y];
                }
            }

            R = (int) (sumR / convolutionMatrix.getFactor() + convolutionMatrix.getOffset());
            if (R < 0) {
                R = 0;
            } else if (R > 255) {
                R = 255;
            }

            G = (int) (sumG / convolutionMatrix.getFactor() + convolutionMatrix.getOffset());
            if (G < 0) {
                G = 0;
            } else if (G > 255) {
                G = 255;
            }

            B = (int) (sumB / convolutionMatrix.getFactor() + convolutionMatrix.getOffset());
            if (B < 0) {
                B = 0;
            } else if (B > 255) {
                B = 255;
            }

            xCord = i + 1;
            yCord = j + 1;
            jPixelsBuffer[xCord + yCord  * width] = Color::argb(A, R, G, B);
        }
    }
}

void PixelProcessing::gaussianBlurPixels(jint *jPixelsBuffer, int width, int height) const {
    double gaussianBlurConfig[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE] = {
            {1, 2, 1},
            {2, 4, 2},
            {1, 2, 1}
    };

    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.applyConfig(gaussianBlurConfig);
    convolutionMatrix.setFactor(16);
    convolutionMatrix.setOffset(0);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::sharpenPixels(jint *jPixelsBuffer, int width, int height, double weight) const {
    double sharpenConfig[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE] = {
            {0,  -2,     0},
            {-2, weight, -2},
            {0,  -2,     0}
    };

    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.applyConfig(sharpenConfig);
    convolutionMatrix.setFactor(weight - 8);
    convolutionMatrix.setOffset(1);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::meanRemovalPixels(jint *jPixelsBuffer, int width, int height) const {
    double meanRemovalConfig[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE] = {
            {-1, -1, -1},
            {-1, 9, -1},
            {-1, -1, -1}
    };
    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.applyConfig(meanRemovalConfig);
    convolutionMatrix.setFactor(1);
    convolutionMatrix.setOffset(0);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::smoothPixels(jint *jPixelsBuffer, int width, int height, jdouble value) const {
    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.setAll(1);
    convolutionMatrix.getMatrix()[1][1] = value;
    convolutionMatrix.setFactor(value + 8);
    convolutionMatrix.setOffset(1);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::embossPixels(jint *jPixelsBuffer, int width, int height) const {
    double embossConfig[CONVOLUTION_MATRIX_SIZE][CONVOLUTION_MATRIX_SIZE] = {
            {-1, 0, -1},
            {0, 4, 0},
            {-1, 0, -1}
    };
    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.applyConfig(embossConfig);
    convolutionMatrix.setFactor(1);
    convolutionMatrix.setOffset(127);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::engravePixels(jint *jPixelsBuffer, int width, int height) const {
    ConvolutionMatrix convolutionMatrix;
    convolutionMatrix.setAll(0);
    convolutionMatrix.getMatrix()[0][0] = -2;
    convolutionMatrix.getMatrix()[1][1] = 2;
    convolutionMatrix.setFactor(1);
    convolutionMatrix.setOffset(95);
    this->computeConvolution3x3(jPixelsBuffer, width, height, convolutionMatrix);
}

void PixelProcessing::boostColorPixels(jint *jPixelsBuffer, int length, int type, double percent) const {
    int A, R, G, B;
    int pixel;

    for (int i = 0; i < length; ++i) {
        pixel = jPixelsBuffer[i];

        A = Color::alpha(pixel);
        R = Color::red(pixel);
        G = Color::green(pixel);
        B = Color::blue(pixel);

        switch (type) {
            case 1:
                R = (int) (R * (1 + percent));
                break;
            case 2:
                G = (int) (G * (1 + percent));
                break;
            case 3:
                B = (int) (B * (1 + percent));
                break;
        }
        jPixelsBuffer[i] = Color::argb(A, R, G, B);
    }
}

void PixelProcessing::tintImagePixels(jint *jPixelsBuffer, int length, int degree) const {
    double fullCircleDegree = 360;
    double halCircleDegree = 180;
    double range = 256;

    int RY, GY, BY, RYY, GYY, BYY, R, G, B, Y;
    double angle = (M_PI * (double) degree) / halCircleDegree;

    int S = (int) (range * sin(angle));
    int C = (int) (range * cos(angle));

    for (int i = 0; i < length; ++i) {
        int r = (jPixelsBuffer[i] >> 16) & 0xff;
        int g = (jPixelsBuffer[i] >> 8) & 0xff;
        int b = jPixelsBuffer[i] & 0xff;

        RY = (70 * r - 59 * g - 11 * b) / 100;
        GY = (-30 * r + 41 * g - 11 * b ) / 100;
        BY = (-30 * r - 59 * g + 89 * b ) / 100;
        Y  = ( 30 * r + 59 * g + 11 * b ) / 100;
        RYY = ( S * BY + C * RY ) / 256;
        BYY = ( C * BY - S * RY ) / 256;
        GYY = (-51 * RYY - 19 * BYY ) / 100;
        R = Y + RYY;
        R = (R < 0) ? 0 : ((R > 255) ? 255 : R);
        G = Y + GYY;
        G = ( G < 0 ) ? 0 : (( G > 255 ) ? 255 : G );
        B = Y + BYY;
        B = ( B < 0 ) ? 0 : (( B > 255 ) ? 255 : B );

        jPixelsBuffer[i] = 0xff000000 | (R << 16) | (G << 8) | B;
    }
}

void PixelProcessing::noisePixels(jint *jPixelsBuffer, int length) const {
    int R, G, B;
    int randomColor;

    for (int i = 0; i < length; ++i) {
        R = rand() % COLOR_MAX;

        G = rand() % COLOR_MAX;

        B = rand() % COLOR_MAX;

        randomColor = Color::rgb(R, G, B);

        jPixelsBuffer[i] |= randomColor;
    }
}

void PixelProcessing::blackFilterPixels(jint *jPixelsBuffer, int length) const {
    int tresHold;

    int R, G, B;

    for (int i = 0; i < length; ++i) {
        R = Color::red(jPixelsBuffer[i]);
        G = Color::green(jPixelsBuffer[i]);
        B = Color::blue(jPixelsBuffer[i]);

        tresHold = rand() % COLOR_MAX;
        if (R < tresHold && G < tresHold && B < tresHold) {
            jPixelsBuffer[i] = Color::rgb(COLOR_MIN, COLOR_MIN, COLOR_MIN);
        }
    }
}

void PixelProcessing::snowFilterPixels(jint *jPixelsBuffer, int length) const {
    int tresHold;

    int R, G, B;

    for (int i = 0; i < length; ++i) {
        R = Color::red(jPixelsBuffer[i]);
        G = Color::green(jPixelsBuffer[i]);
        B = Color::blue(jPixelsBuffer[i]);

        tresHold = rand() % COLOR_MAX;

        if (R > tresHold && G > tresHold && B > tresHold) {
            jPixelsBuffer[i] = Color::rgb(COLOR_MAX, COLOR_MAX, COLOR_MAX);
        }
    }
}

void PixelProcessing::shadingFilterPixels(jint *jPixelsBuffer, int length, double red, double green, double blue) const {
    for (int i = 0; i < length; ++i) {
        jPixelsBuffer[i] &= Color::rgb(red, green, blue);
    }
}

void PixelProcessing::applySaturationPixels(jint *jPixelsBuffer, int length, int level) const {
    float HSV[3];

    int RGB[3];

    for (int i = 0; i < length; ++i) {
        RGB[0] = Color::red(jPixelsBuffer[i]);
        RGB[1] = Color::green(jPixelsBuffer[i]);
        RGB[2] = Color::blue(jPixelsBuffer[i]);

        Color::RGBToHSV(RGB, HSV);

        HSV[1] *= level;
        HSV[1] = std::max(0.0f, std::min(HSV[1], 1.0f));

        Color::HSVToRGB(HSV, RGB);

        jPixelsBuffer[i] |= Color::rgb(RGB[0], RGB[1], RGB[2]);
    }
}

void PixelProcessing::applyHuePixels(jint *jPixelsBuffer, int length, int level) const {
    float HSV[3];

    int RGB[3];

    for (int i = 0; i < length; ++i) {
        RGB[0] = Color::red(jPixelsBuffer[i]);
        RGB[1] = Color::green(jPixelsBuffer[i]);
        RGB[2] = Color::blue(jPixelsBuffer[i]);

        Color::RGBToHSV(RGB, HSV);

        HSV[0] *= level;
        HSV[0] = std::max(0.0f, std::min(HSV[0], 360.0f));

        Color::HSVToRGB(HSV, RGB);

        jPixelsBuffer[i] |= Color::rgb(RGB[0], RGB[1], RGB[2]);
    }
}
