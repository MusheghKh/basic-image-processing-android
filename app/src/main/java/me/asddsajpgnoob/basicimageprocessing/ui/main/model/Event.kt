package me.asddsajpgnoob.basicimageprocessing.ui.main.model

import android.graphics.Bitmap

sealed interface Event {

    data class ShowToastString(val string: String) : Event

    data class SaveBitmapToGallery(val bitmap: Bitmap) : Event

    data class ShowPickRGBDialog(val processingType: ImageProcessingType) : Event
}