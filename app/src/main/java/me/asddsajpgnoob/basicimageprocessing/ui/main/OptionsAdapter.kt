package me.asddsajpgnoob.basicimageprocessing.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import me.asddsajpgnoob.basicimageprocessing.databinding.ItemOptionBinding
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.ImageProcessingType

class OptionsAdapter(
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<OptionsAdapter.OptionViewHolder>() {

    val items = mutableListOf<ImageProcessingType>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionViewHolder {
        val binding = ItemOptionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OptionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class OptionViewHolder(
        private val binding: ItemOptionBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val pos = adapterPosition
                if (pos != NO_POSITION) {
                    listener.onItemClick(items[pos])
                }
            }
        }

        fun bind(item: ImageProcessingType) {
            binding.textName.text = item.title
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: ImageProcessingType)
    }
}