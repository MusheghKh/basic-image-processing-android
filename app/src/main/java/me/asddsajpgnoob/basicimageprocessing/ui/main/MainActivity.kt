package me.asddsajpgnoob.basicimageprocessing.ui.main

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.basicimageprocessing.R
import me.asddsajpgnoob.basicimageprocessing.UnexpectedException
import me.asddsajpgnoob.basicimageprocessing.databinding.ActivityMainBinding
import me.asddsajpgnoob.basicimageprocessing.ui.dialog.PickRGBDialog
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.Event
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.ImageProcessingType
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.State
import me.asddsajpgnoob.basicimageprocessing.util.saveBitmapInAppPicturesFolder
import java.io.IOException

class MainActivity : AppCompatActivity(), OptionsAdapter.OnItemClickListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var viewModel: MainViewModel

    private var itemUndo: MenuItem? = null
    private var itemSave: MenuItem? = null
    private var itemChooseAnotherImage: MenuItem? = null

    private val optionsAdapter = OptionsAdapter(this)

    private val pickImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                val uri: Uri? = it.data?.data

                if (uri == null) {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
                    return@registerForActivityResult
                }

                val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    val source = ImageDecoder.createSource(contentResolver, uri)
                    ImageDecoder.decodeBitmap(source).copy(Bitmap.Config.ARGB_8888, true)
                } else {
                    MediaStore.Images.Media.getBitmap(contentResolver, uri)
                }

                viewModel.onImagePicked(bitmap)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        setupRecyclerViews()

        setOnClickListeners()
        collectState()
        collectEvents()
    }

    private fun setupRecyclerViews() {
        binding.apply {
            recyclerViewOptions.setHasFixedSize(true)
            recyclerViewOptions.adapter = optionsAdapter

            val items = enumValues<ImageProcessingType>()
            optionsAdapter.items.addAll(items)
            optionsAdapter.notifyItemRangeInserted(0, items.size)
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            buttonSelectImage.setOnClickListener {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                pickImageResult.launch(i)
            }
        }
    }

    private fun collectState() {
        lifecycleScope.launchWhenStarted {
            viewModel.state.collectLatest { state ->
                binding.apply {
                    when (state) {
                        State.NothingToShow -> {
                            progress.isVisible = false
                            buttonSelectImage.isVisible = true
                            recyclerViewOptions.isVisible = false
                            image.isVisible = false

                            itemSave?.isVisible = false
                            itemUndo?.isVisible = false
                            itemChooseAnotherImage?.isVisible = false
                        }
                        is State.ImageLoaded -> {
                            progress.isVisible = false
                            buttonSelectImage.isVisible = false
                            recyclerViewOptions.isVisible = true
                            image.isVisible = true

                            image.setImageBitmap(state.loadedBitmap)

                            itemSave?.isVisible = false
                            itemUndo?.isVisible = false
                            itemChooseAnotherImage?.isVisible = true
                        }
                        is State.ImageProceed -> {
                            progress.isVisible = false
                            buttonSelectImage.isVisible = false
                            recyclerViewOptions.isVisible = true
                            image.isVisible = true

                            image.setImageBitmap(state.resultBitmap)

                            itemSave?.isVisible = true
                            itemUndo?.isVisible = true
                            itemChooseAnotherImage?.isVisible = true
                        }
                        State.Loading -> {
                            progress.isVisible = true
                            buttonSelectImage.isVisible = false
                            recyclerViewOptions.isVisible = false
                            image.isVisible = false

                            itemSave?.isVisible = false
                            itemUndo?.isVisible = false
                            itemChooseAnotherImage?.isVisible = false
                        }
                    }
                }
            }
        }
    }

    private fun collectEvents() {
        lifecycleScope.launchWhenStarted {
            viewModel.events.collect { event ->
                when (event) {
                    is Event.ShowToastString -> {
                        Toast.makeText(this@MainActivity, event.string, Toast.LENGTH_SHORT).show()
                    }
                    is Event.ShowPickRGBDialog -> {
                        val dialog = PickRGBDialog(
                            this@MainActivity,
                            event.processingType
                        ) { type, intValue, doubleValue, red, green, blue ->
                            viewModel.onPickRGb(type, intValue, doubleValue, red, green, blue)
                        }
                        dialog.show()
                    }
                    is Event.SaveBitmapToGallery -> {
                        val success = try {
                            event.bitmap.saveBitmapInAppPicturesFolder(contentResolver)
                            true
                        } catch (e: UnexpectedException) {
                            false
                        } catch (e: IOException) {
                            false
                        }
                        if (success) {
                            Toast.makeText(this@MainActivity, "Saved", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this@MainActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        itemUndo = menu?.findItem(R.id.item_reset)
        itemSave = menu?.findItem(R.id.item_save)
        itemChooseAnotherImage = menu?.findItem(R.id.item_choose_another_image)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_save -> {
                viewModel.onMenuItemSaveSelected()
                true
            }
            R.id.item_reset -> {
                viewModel.onMenuItemUndoSelected()
                true
            }
            R.id.item_choose_another_image -> {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                pickImageResult.launch(i)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onItemClick(item: ImageProcessingType) {
        viewModel.onOptionClicked(item)
    }
}