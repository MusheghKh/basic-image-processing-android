package me.asddsajpgnoob.basicimageprocessing.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import androidx.core.view.isVisible
import me.asddsajpgnoob.basicimageprocessing.databinding.DialogPickRgbBinding
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.ImageProcessingType
import me.asddsajpgnoob.basicimageprocessing.util.zeroDoubleIfNumberFormatError
import me.asddsajpgnoob.basicimageprocessing.util.zeroIntIfNumberFormatError

class PickRGBDialog(
    context: Context,
    private val processingType: ImageProcessingType,
    private val listener: OnSubmitListener
) : Dialog(context) {

    constructor(
        context: Context,
        processingType: ImageProcessingType,
        listener: (processingType: ImageProcessingType, intValue: Int, doubleValue: Double, red: Double, green: Double, blue: Double) -> Unit
    ) : this(
        context,
        processingType,
        object : OnSubmitListener {
            override fun onSubmit(
                processingType: ImageProcessingType,
                intValue: Int,
                doubleValue: Double,
                red: Double,
                green: Double,
                blue: Double
            ) {
                listener.invoke(processingType, intValue, doubleValue, red, green, blue)
            }
        }
    )

    private lateinit var binding: DialogPickRgbBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogPickRgbBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViews()
        setOnCLickListener()

        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setupViews() {
        binding.apply {
            when (processingType) {
                ImageProcessingType.GAMMA -> {
                    editTextRed.isVisible = true
                    editTextGreen.isVisible = true
                    editTextBlue.isVisible = true
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.COLOR_FILTER -> {
                    editTextRed.isVisible = true
                    editTextGreen.isVisible = true
                    editTextBlue.isVisible = true
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.SEPIA_TONING -> {
                    editTextRed.isVisible = true
                    editTextGreen.isVisible = true
                    editTextBlue.isVisible = true
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Depth (int)"
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.COLOR_DEPTH -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Bit offset (int)"
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.CREATE_CONTRAST -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = true
                    editTextDoubleField.hint = "Contrast (double)"
                }
                ImageProcessingType.BRIGHTNESS -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Brightness (int)"
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.SHARPEN -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = true
                    editTextDoubleField.hint = "Weight (double)"
                }
                ImageProcessingType.SMOOTH -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = true
                    editTextDoubleField.hint = "Value (double)"
                }
                ImageProcessingType.BOOST_COLOR -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Color index (int)"
                    editTextDoubleField.isVisible = true
                    editTextDoubleField.hint = "Percent (double)"
                }
                ImageProcessingType.TINT_IMAGE -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Degree (int)"
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.SHADING_FILTER -> {
                    editTextRed.isVisible = true
                    editTextGreen.isVisible = true
                    editTextBlue.isVisible = true
                    editTextIntField.isVisible = false
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.SATURATION -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Level (int)"
                    editTextDoubleField.isVisible = false
                }
                ImageProcessingType.HUE -> {
                    editTextRed.isVisible = false
                    editTextGreen.isVisible = false
                    editTextBlue.isVisible = false
                    editTextIntField.isVisible = true
                    editTextIntField.hint = "Level (int)"
                    editTextDoubleField.isVisible = false
                }
                else -> {
                    throw RuntimeException("no such option")
                }
            }
        }
    }

    private fun setOnCLickListener() {
        binding.apply {
            buttonSubmit.setOnClickListener {
                listener.onSubmit(
                    processingType,
                    zeroIntIfNumberFormatError {
                        editTextIntField.text.toString().trim().toInt()
                    },
                    zeroDoubleIfNumberFormatError {
                        editTextDoubleField.text.toString().trim().toDouble()
                    },
                    zeroDoubleIfNumberFormatError {
                        editTextRed.text.toString().trim().toDouble()
                    },
                    zeroDoubleIfNumberFormatError {
                        editTextGreen.text.toString().trim().toDouble()
                    },
                    zeroDoubleIfNumberFormatError {
                        editTextBlue.text.toString().trim().toDouble()
                    }
                )
                dismiss()
            }
        }
    }

    @FunctionalInterface
    interface OnSubmitListener {
        fun onSubmit(
            processingType: ImageProcessingType,
            intValue: Int,
            doubleValue: Double,
            red: Double,
            green: Double,
            blue: Double
        )
    }
}