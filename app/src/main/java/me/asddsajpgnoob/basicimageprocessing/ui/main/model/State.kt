package me.asddsajpgnoob.basicimageprocessing.ui.main.model

import android.graphics.Bitmap

sealed interface State {

    object NothingToShow : State

    object Loading : State

    data class ImageLoaded(val loadedBitmap: Bitmap) : State

    data class ImageProceed(val loadedBitmap: Bitmap, val resultBitmap: Bitmap) : State

}