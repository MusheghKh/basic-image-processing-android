package me.asddsajpgnoob.basicimageprocessing.ui.main

import android.graphics.Bitmap
import android.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.Event
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.ImageProcessingType
import me.asddsajpgnoob.basicimageprocessing.ui.main.model.State
import me.asddsajpgnoob.basicimageprocessing.util.BitmapProcessingUtils

class MainViewModel : ViewModel() {

    companion object {
        init {
            System.loadLibrary("myapplication")
        }
    }

    private val _state = MutableStateFlow<State>(State.NothingToShow)
    val state = _state.asStateFlow()

    private val eventsChannel = Channel<Event>()
    val events = eventsChannel.receiveAsFlow()

    fun onImagePicked(image: Bitmap) {
        _state.value = State.ImageLoaded(image)
    }

    fun onMenuItemUndoSelected() {
        val currentState = _state.value
        if (currentState is State.ImageProceed) {
            _state.value = State.ImageLoaded(currentState.loadedBitmap)
        }
    }

    fun onMenuItemSaveSelected() = viewModelScope.launch {
        val currentState = _state.value
        if (currentState is State.ImageProceed) {
            eventsChannel.send(Event.SaveBitmapToGallery(currentState.resultBitmap))
        }
    }

    fun onPickRGb(
        type: ImageProcessingType,
        intValue: Int,
        doubleValue: Double,
        red: Double,
        green: Double,
        blue: Double
    ) {
        when (type) {
            ImageProcessingType.GAMMA -> {
                doGamma(red, green, blue)
            }
            ImageProcessingType.COLOR_FILTER -> {
                doColorFilter(red, green, blue)
            }
            ImageProcessingType.SEPIA_TONING -> {
                doSepiaToning(intValue, red, green, blue)
            }
            ImageProcessingType.COLOR_DEPTH -> {
                doDecreaseColorDepth(intValue)
            }
            ImageProcessingType.CREATE_CONTRAST -> {
                doCreateContrast(doubleValue)
            }
            ImageProcessingType.BRIGHTNESS -> {
                doBrightness(intValue)
            }
            ImageProcessingType.SHARPEN -> {
                doSharpen(doubleValue)
            }
            ImageProcessingType.SMOOTH -> {
                doSmooth(doubleValue)
            }
            ImageProcessingType.BOOST_COLOR -> {
                if (!(1..3).contains(intValue)) {
                    viewModelScope.launch {
                        eventsChannel.send(Event.ShowToastString("color index can be 1, 2 or 3"))
                    }
                    return
                }
                if (!(0.0..100.0).contains(doubleValue)) {
                    viewModelScope.launch {
                        eventsChannel.send(Event.ShowToastString("percent must be in 0..100"))
                    }
                    return
                }
                doBoostColor(intValue, doubleValue)
            }
            ImageProcessingType.TINT_IMAGE -> {
                doTintImage(intValue)
            }
            ImageProcessingType.SHADING_FILTER -> {
                doShadingFilter(red, green, blue)
            }
            ImageProcessingType.SATURATION -> {
                doSaturation(intValue)
            }
            ImageProcessingType.HUE -> {
                doHue(intValue)
            }
            else -> {
                // do not nothing
            }
        }
    }

    fun onOptionClicked(item: ImageProcessingType) {
        when (item) {
            ImageProcessingType.INVERT -> {
                invertImage()
            }
            ImageProcessingType.GREY_SCALE -> {
                greyScaleImage()
            }
            ImageProcessingType.GAMMA -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.COLOR_FILTER -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.SEPIA_TONING -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.COLOR_DEPTH -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.CREATE_CONTRAST -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.BRIGHTNESS -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.GAUSSIAN_BLUR -> {
                doGaussianBlur()
            }
            ImageProcessingType.SHARPEN -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.MEAD_REMOVAL -> {
                doMeanRemoval()
            }
            ImageProcessingType.SMOOTH -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.EMBOSS -> {
                doEmboss()
            }
            ImageProcessingType.ENGRAVE -> {
                doEngrave()
            }
            ImageProcessingType.BOOST_COLOR -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.TINT_IMAGE -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.NOISE -> {
                doNoise()
            }
            ImageProcessingType.BLACK_FILTER -> {
                doBlackFilter()
            }
            ImageProcessingType.SNOW_FILTER -> {
                doSnowFilter()
            }
            ImageProcessingType.SHADING_FILTER -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.SATURATION -> {
                openPickRGBDialog(item)
            }
            ImageProcessingType.HUE -> {
                openPickRGBDialog(item)
            }
        }
    }

    private fun openPickRGBDialog(item:  ImageProcessingType) = viewModelScope.launch {
        viewModelScope.launch {
            eventsChannel.send(Event.ShowPickRGBDialog(item))
        }
    }

    private fun invertImage() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.invertBitmap(it)
        }
    }

    private fun greyScaleImage() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.greyScaleBitmap(it)
        }
    }

    private fun doGamma(red: Double, green: Double, blue: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doGammaBitmap(it, red, green, blue)
        }
    }

    private fun doColorFilter(red: Double, green: Double, blue: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doColorFilterBitmap(it, red, green, blue)
        }
    }

    private fun doSepiaToning(depth: Int, red: Double, green: Double, blue: Double) =
        viewModelScope.launch {
            proceedBitmap {
                BitmapProcessingUtils.doSepiaToningBitmap(it, depth, red, green, blue)
            }
        }

    private fun doDecreaseColorDepth(bitOffset: Int) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.decreaseColorDepthBitmap(it, bitOffset)
        }
    }

    private fun doCreateContrast(value: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doContrastBitmap(it, value)
        }
    }

    private fun doBrightness(value: Int) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doBrightnessBitmap(it, value)
        }
    }

    private fun doGaussianBlur() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doGaussianBlurBitmap(it)
        }
    }

    private fun doSharpen(weight: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doSharpenBitmap(it, weight)
        }
    }

    private fun doMeanRemoval() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doMeanRemovalBitmap(it)
        }
    }

    private fun doSmooth(value: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doSmoothBitmap(it, value)
        }
    }

    private fun doEmboss() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doEmbossBitmap(it)
        }
    }

    private fun doEngrave() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doEngraveBitmap(it)
        }
    }

    private fun doBoostColor(type: Int, percent: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doBoostColorBitmap(it, type, percent)
        }
    }

    private fun doTintImage(degree: Int) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doTintBitmap(it, degree)
        }
    }

    private fun doNoise() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doNoiseBitmap(it)
        }
    }

    private fun doBlackFilter() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doBlackFilterBitmap(it)
        }
    }

    private fun doSnowFilter() = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doSnowFilterBitmap(it)
        }
    }

    private fun doShadingFilter(red: Double, green: Double, blue: Double) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doShadingFilterBitmap(it, red, green, blue)
        }
    }

    private fun doSaturation(level: Int) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doSaturationBitmap(it, level)
        }
    }

    private fun doHue(level: Int) = viewModelScope.launch {
        proceedBitmap {
            BitmapProcessingUtils.doHueBitmap(it, level)
        }
    }

    private fun getLoadedBitmapOrNull() = when (val currentState = _state.value) {
        is State.ImageLoaded -> {
            currentState.loadedBitmap
        }
        is State.ImageProceed -> {
            currentState.loadedBitmap
        }
        else -> {
            null
        }
    }

    private suspend inline fun proceedBitmap(crossinline process: (initialBitmap: Bitmap) -> Bitmap) {
        val loadedBitmap = getLoadedBitmapOrNull() ?: return

        _state.value = State.Loading

        val finalBitmap = withContext(Dispatchers.Default) {
            process.invoke(loadedBitmap)
        }

        _state.value = State.ImageProceed(loadedBitmap, finalBitmap)
    }
}