package me.asddsajpgnoob.basicimageprocessing.ui.main.model

enum class ImageProcessingType(val title: String) {
    INVERT("Negative"),
    GREY_SCALE("Grey"),
    GAMMA("Gamma"),
    COLOR_FILTER("Color Filter"),
    SEPIA_TONING("Sepia"),
    COLOR_DEPTH("Color depth"),
    CREATE_CONTRAST("Contrast"),
    BRIGHTNESS("Brightness"),
    GAUSSIAN_BLUR("Blur"),
    SHARPEN("Sharpen"),
    MEAD_REMOVAL("Mean remove"),
    SMOOTH("Smooth"),
    EMBOSS("Emboss"),
    ENGRAVE("Engrave"),
    BOOST_COLOR("Boost color"),
    TINT_IMAGE("Tint image"),
    NOISE("Noise"),
    BLACK_FILTER("Black filter"),
    SNOW_FILTER("Snow filter"),
    SHADING_FILTER("Shading"),
    SATURATION("Saturation"),
    HUE("Hue")
}