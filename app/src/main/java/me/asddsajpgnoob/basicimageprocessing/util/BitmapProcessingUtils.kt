package me.asddsajpgnoob.basicimageprocessing.util

import android.graphics.Bitmap

object BitmapProcessingUtils {

    fun invertBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            invertPixels(pixels)
        }
    }

    fun greyScaleBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            greyScalePixels(pixels)
        }
    }

    fun doGammaBitmap(bitmap: Bitmap, red: Double, green: Double, blue: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            doGamma(pixels, red, green, blue)
        }
    }

    fun doColorFilterBitmap(bitmap: Bitmap, red: Double, green: Double, blue: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            doColorFilterPixels(pixels, red, green, blue)
        }
    }

    fun doSepiaToningBitmap(
        bitmap: Bitmap,
        depth: Int,
        red: Double,
        green: Double,
        blue: Double
    ): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            doSepiaToningPixels(pixels, depth, red, green, blue)
        }
    }

    fun decreaseColorDepthBitmap(bitmap: Bitmap, bitOffset: Int): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            decreaseColorDepthPixels(pixels, bitOffset)
        }
    }

    fun doContrastBitmap(bitmap: Bitmap, value: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            createContrastPixels(pixels, value)
        }
    }

    fun doBrightnessBitmap(bitmap: Bitmap, value: Int): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            brightnessPixels(pixels, value)
        }
    }

    fun doGaussianBlurBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            gaussianBlurPixels(pixels, width, height)
        }
    }

    fun doSharpenBitmap(bitmap: Bitmap, weight: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            sharpenPixels(pixels, width, height, weight)
        }
    }

    fun doMeanRemovalBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            meanRemovalPixels(pixels, width, height)
        }
    }

    fun doSmoothBitmap(bitmap: Bitmap, value: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            smoothPixels(pixels, width, height, value)
        }
    }

    fun doEmbossBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            embossPixels(pixels, width, height)
        }
    }

    fun doEngraveBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, width, height ->
            engravePixels(pixels, width, height)
        }
    }

    fun doBoostColorBitmap(bitmap: Bitmap, type: Int, percent: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            boostColorPixels(pixels, type, percent)
        }
    }

    fun doTintBitmap(bitmap: Bitmap, degree: Int): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            tintImagePixels(pixels, degree)
        }
    }

    fun doNoiseBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            noisePixels(pixels)
        }
    }

    fun doBlackFilterBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            blackFilterPixels(pixels)
        }
    }

    fun doSnowFilterBitmap(bitmap: Bitmap): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            snowFilterPixels(pixels)
        }
    }

    fun doShadingFilterBitmap(bitmap: Bitmap, red: Double, green: Double, blue: Double): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            shadingFilterPixels(pixels, red, green, blue)
        }
    }

    fun doSaturationBitmap(bitmap: Bitmap, level: Int): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            applySaturationPixels(pixels, level)
        }
    }

    fun doHueBitmap(bitmap: Bitmap, level: Int): Bitmap {
        return createProceedBitmap(bitmap) { pixels, _, _ ->
            applyHuePixels(pixels, level)
        }
    }

    private inline fun createProceedBitmap(bitmap: Bitmap, process: (pixels: IntArray, width: Int, height: Int) -> Unit): Bitmap {
        val pixels = bitmap.getPixelsForImageProcessing()
        val height = bitmap.height
        val width = bitmap.width
        process.invoke(pixels, bitmap.width, bitmap.height)
        return BitmapUtils.createBitmapAndSetPixels(pixels, width, height, Bitmap.Config.ARGB_8888)
    }

    private external fun invertPixels(pixels: IntArray)

    private external fun greyScalePixels(pixels: IntArray)

    private external fun doGamma(
        pixels: IntArray,
        red: Double,
        green: Double,
        blue: Double
    )

    private external fun doColorFilterPixels(
        pixels: IntArray,
        red: Double,
        green: Double,
        blue: Double
    )

    private external fun doSepiaToningPixels(
        pixels: IntArray,
        depth: Int,
        red: Double,
        green: Double,
        blue: Double
    )

    private external fun decreaseColorDepthPixels(pixels: IntArray, bitOffset: Int)

    private external fun createContrastPixels(pixels: IntArray, value: Double)

    private external fun brightnessPixels(pixels: IntArray, value: Int)

    private external fun gaussianBlurPixels(pixels: IntArray, width: Int, height: Int)

    private external fun sharpenPixels(pixels: IntArray, width: Int, height: Int, weight: Double)

    private external fun meanRemovalPixels(pixels: IntArray, width: Int, height: Int)

    private external fun smoothPixels(pixels: IntArray, width: Int, height: Int, value: Double)

    private external fun embossPixels(pixels: IntArray, width: Int, height: Int)

    private external fun engravePixels(pixels: IntArray, width: Int, height: Int)

    private external fun boostColorPixels(pixels: IntArray, type: Int, percent: Double)

    private external fun tintImagePixels(pixels: IntArray, degree: Int)

    private external fun noisePixels(pixels: IntArray)

    private external fun blackFilterPixels(pixels: IntArray)

    private external fun snowFilterPixels(pixels: IntArray)

    private external fun shadingFilterPixels(pixels: IntArray, red: Double, green: Double, blue: Double)

    private external fun applySaturationPixels(pixels: IntArray, level: Int)

    private external fun applyHuePixels(pixels: IntArray, level: Int)
}