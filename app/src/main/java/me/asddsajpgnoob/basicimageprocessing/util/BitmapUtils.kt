package me.asddsajpgnoob.basicimageprocessing.util

import android.graphics.Bitmap

object BitmapUtils {
    fun createBitmapAndSetPixels(pixels: IntArray, width: Int, height: Int, config: Bitmap.Config): Bitmap {
        return Bitmap.createBitmap(width, height, config).apply {
            setPixels(pixels, 0, width, 0, 0, width, height)
        }
    }
}

fun Bitmap.getPixelsForImageProcessing(): IntArray {
    val pixels = IntArray(width * height)
    getPixels(pixels, 0, width, 0, 0, width, height)
    return pixels
}