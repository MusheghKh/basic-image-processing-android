package me.asddsajpgnoob.basicimageprocessing.util

import android.content.ContentResolver
import android.content.ContentValues
import android.graphics.Bitmap
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import me.asddsajpgnoob.basicimageprocessing.UnexpectedException
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.util.*

fun File.writeBitmap(bitmap: Bitmap, format: Bitmap.CompressFormat, quality: Int) {
    outputStream().writeBitmap(bitmap, format, quality)
}

fun OutputStream.writeBitmap(bitmap: Bitmap, format: Bitmap.CompressFormat, quality: Int) {
    bitmap.compress(format, quality, this)
    close()
}

@Throws(UnexpectedException::class, IOException::class)
fun Bitmap.saveBitmapInAppPicturesFolder(contentResolver: ContentResolver) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val values = ContentValues()
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, "${UUID.randomUUID()}.jpeg")
        values.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
        values.put(
            MediaStore.MediaColumns.RELATIVE_PATH,
            "${Environment.DIRECTORY_PICTURES}/BIP"
        )

        val uri = contentResolver.insert(
            MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL),
            values
        ) ?: throw UnexpectedException()

        val outputStream = contentResolver.openOutputStream(uri) ?: throw UnexpectedException()

        outputStream.writeBitmap(this, Bitmap.CompressFormat.JPEG, 100)
        return
    }

    val saveDir =
        File("${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath}/BIP")
    if (!saveDir.isDirectory) {
        saveDir.mkdir()
    }
    val saveFile = File("${saveDir.absolutePath}/${UUID.randomUUID()}.jpeg")
    saveFile.writeBitmap(this, Bitmap.CompressFormat.JPEG, 100)
}