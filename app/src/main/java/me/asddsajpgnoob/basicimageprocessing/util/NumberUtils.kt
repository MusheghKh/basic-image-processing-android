package me.asddsajpgnoob.basicimageprocessing.util

import java.lang.NumberFormatException

fun zeroIntIfNumberFormatError(get: () -> Int): Int {
    return try {
        get.invoke()
    } catch (e: NumberFormatException) {
        0
    }
}

fun zeroDoubleIfNumberFormatError(get: () -> Double): Double {
    return try {
        get.invoke()
    } catch (e: NumberFormatException) {
        0.0
    }
}